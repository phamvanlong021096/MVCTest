Nettuts+ Article - Build a Complete MVC Web Site With ExpressJS

Technology Workshop assignment: https://code.tutsplus.com/tutorials/build-a-complete-mvc-website-with-expressjs--net-34168

Installation

- Download the source code
- Go to app directory and run npm install
- Run mongodb daemon
- Run the app: npm start
- Open http://localhost:3000 (for the front-end)
- Open http://localhost:3000/admin (for the control panel)

Enjoy !